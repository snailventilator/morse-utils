package snailventilator.morseUtils.converter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TextConverter {
	private InternalMorseConverter imc;

	public TextConverter() {
		this.imc = new InternalMorseConverter();
	}

	public String encode(String input) {
		char[] charArray = input.toCharArray();

		boolean[] booleanArray = imc.encode(charArray);

		String rawResultString = IntStream.range(0, booleanArray.length).mapToObj(i -> booleanArray[i] ? "1" : "0").collect(Collectors.joining());
		rawResultString = rawResultString.replaceAll("0000000", " / ");
		rawResultString = rawResultString.replaceAll("000", " ");
		rawResultString = rawResultString.replaceAll("111", "-");
		rawResultString = rawResultString.replaceAll("1", ".");
		rawResultString = rawResultString.replaceAll("0", "");
		return rawResultString;
	}

	public String decode(String input) {
		String[] spaceSplit = input.split(" ");
		for(int i1 = 0; i1 < spaceSplit.length; i1++) {
			char[] chars = spaceSplit[i1].toCharArray();
			char[] target = new char[chars.length * 2 - 1];
			for(int i = 0; i < chars.length; i++) {
				target[i * 2] = chars[i];
				if(i != chars.length - 1) {
					target[i * 2 + 1] = '0';
				}
			}
			spaceSplit[i1] = new String(target);
		}
		input = String.join(" ", spaceSplit);

		input = input.replaceAll("\\.", "1");
		input = input.replaceAll("-", "111");
		input = input.replaceAll(" ", "000");
		input = input.replaceAll(" / ", "00000000");

		List<Boolean> inputList = input.chars().mapToObj(character -> character == '1').collect(Collectors.toList());
		boolean[] inputArray = new boolean[inputList.size()];
		for(int i = 0; i < inputList.size(); i++) {
			inputArray[i] = inputList.get(i);
		}

		char[] resultArray = imc.decode(inputArray);

		return new String(resultArray);
	}
}
