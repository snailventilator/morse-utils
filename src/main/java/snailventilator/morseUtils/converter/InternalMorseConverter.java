package snailventilator.morseUtils.converter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//TODO: Rewrite with streams as parameters instead of arrays.
class InternalMorseConverter {
	private final Map<Character, List<Boolean>> encodeAlphabet;
	private final Map<List<Boolean>, Character> decodeAlphabet;

	InternalMorseConverter() {
		decodeAlphabet = new HashMap<>();
		encodeAlphabet = new HashMap<>();
		InputStream morseStream = InternalMorseConverter.class.getClassLoader().getResourceAsStream("morse.txt");
		assert morseStream != null; //FIXME
		BufferedReader reader = new BufferedReader(new InputStreamReader(morseStream));
		reader.lines().forEach(line -> {
			if(line.startsWith("#"))
				return;
			Character key = line.charAt(0);
			List<Boolean> value = line.substring(2).codePoints().mapToObj(character -> character == '1').collect(Collectors.toList());
			encodeAlphabet.put(key, value);
			decodeAlphabet.put(value, key);
		});
	}

	boolean[] encode(char[] content) {
		List<Boolean> result = new ArrayList<>();

		for(char c : content) {
			result.addAll((encodeAlphabet.get(Character.toLowerCase(c))));
			result.add(false);
			result.add(false);
			result.add(false);
		}
		//Remove last whitespace
		result = result.stream().limit(result.size() - 3).collect(Collectors.toList());

		boolean[] resultArray = new boolean[result.size()];
		for(int i = 0; i < result.size(); i++) {
			resultArray[i] = result.get(i);
		}
		return resultArray;
	}

	char[] decode(boolean[] rawContent) {
		List<Character> result = new ArrayList<>();

		//Convert boolean array to string with 0 and 1
		String content = IntStream.range(0, rawContent.length).mapToObj(i -> rawContent[i] ? "1" : "0").collect(Collectors.joining());

		String[] words = content.split("0000000");
		for(String word : words) {
			String[] letters = word.split("000");
			for(String letter : letters) {
				List<Boolean> boolLetter = letter.chars().mapToObj(character -> character == '1').collect(Collectors.toList());
				result.add(decodeAlphabet.get(boolLetter));
			}
			result.add(' ');
		}
		//Remove last whitespace
		result.remove(result.size() - 1);

		char[] resultArray = new char[result.size()];
		for(int i = 0; i < resultArray.length; i++) {
			resultArray[i] = result.get(i);
		}
		return resultArray;
	}
}
