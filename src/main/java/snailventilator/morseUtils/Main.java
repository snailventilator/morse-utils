package snailventilator.morseUtils;

import snailventilator.morseUtils.converter.TextConverter;

public class Main {
	public static void main(String[] args) {
		TextConverter tc = new TextConverter();
		System.out.println(tc.decode("- .... .. ... / .. ... / .- / - . ... - -.-.--"));
		System.out.println(tc.encode("This is a test!"));
	}
}
