package snailventilator.morseUtils.converter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TextConverterTest {
	@Test
	public void encode() {
		TextConverter tc = new TextConverter();
		String result = tc.encode("asdf asdf");
		System.out.println(result);
		assertEquals(".- ... -.. ..-. / .- ... -.. ..-.", result);
	}

	@Test
	public void decode() {
		TextConverter tc = new TextConverter();
		String result = tc.decode(".- ... -.. ..-. / .- ... -.. ..-.");
		System.out.println(result);
		assertEquals("asdf asdf", result);
	}
}