package snailventilator.morseUtils.converter;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class InternalMorseConverterTest {
	@Test
	public void encode() {
		InternalMorseConverter imc = new InternalMorseConverter();
		boolean[] result = imc.encode("test".toCharArray());
		System.out.println(Arrays.toString(result));
		assertArrayEquals(new boolean[]{true, true, true, false, false, false, true, false, false, false, true, false, true, false, true, false, false, false, true, true, true}, result);
	}

	@Test
	public void decode() {
		InternalMorseConverter imc = new InternalMorseConverter();
		//"test" in morse
		boolean[] test = {true, true, true, false, false, false, true, false, false, false, true, false, true, false, true, false, false, false, true, true, true};
		char[] result = imc.decode(test);
		System.out.println(Arrays.toString(result));
		assertArrayEquals(new char[]{'t', 'e', 's', 't'}, result);
	}
}